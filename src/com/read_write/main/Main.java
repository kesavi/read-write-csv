package com.read_write.main;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    static String[] headerRow;
    static String fieldName;
    static String fieldValue;
    static String filePath;
    static BufferedReader csvReader = null;

    public static void main(String[] args) throws IOException {
        getInput();
        setReader();
        setHeaderRow();
        findRecord();
        csvReader.close();
    }

    static void findRecord() throws IOException {
        int fieldIndex = getIndex();
        Boolean resultFound = false;
        String row = "";
        while ((row = csvReader.readLine()) != null) {
            String[] result = row.split(",");
            try{
                if (result[fieldIndex].equals(fieldValue)) {
                    resultFound = true;
                    JSONObject json = new JSONObject();
                    for (int index = 0; index < headerRow.length; index++) {
                        json.put(headerRow[index], result[index]);
                    }
                    System.out.println(json.toString());
                    break;
                }
            } catch (Exception e) {
                System.out.println("Incorrect field name. The fields on the file are "+ Arrays.toString(headerRow));
                getInput();
                setReader();
            }
        }
        if(!resultFound) {
            System.out.println("We could not find a match for entered value");
        }
    }

    static void setReader() {
        try {
            csvReader = new BufferedReader(new FileReader(filePath));
        } catch (Exception e) {
            System.out.println("Unable to read the file. Please try again with a different file");
            getInput();
            setReader();
        } finally {


        }
    }

    static void setHeaderRow() throws IOException {
        headerRow = csvReader.readLine().split(",");
    }

    static int getIndex() {
        int index;
        for (index = 0; index < Main.headerRow.length; index++) {
            if (headerRow[index].equals(Main.fieldName)) {
                break;
            }
        }
        return index;
    }

    static void getInput() {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter file path:");
        filePath = input.next();
        System.out.print("Enter field name:");
        fieldName = input.next();
        System.out.print("Enter student field value:");
        fieldValue = input.next();
        System.out.println("Your choice is: filepath" + filePath + " fieldName: " + fieldName + " fieldValue: " + fieldValue);
    }
}